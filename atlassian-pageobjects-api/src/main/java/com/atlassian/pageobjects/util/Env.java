package com.atlassian.pageobjects.util;

public class Env {

    public static String getVar(String key, String _default) {
        String var = System.getenv(key);
        if(var == null) {
            return _default;
        }
        return var;
    }
}
