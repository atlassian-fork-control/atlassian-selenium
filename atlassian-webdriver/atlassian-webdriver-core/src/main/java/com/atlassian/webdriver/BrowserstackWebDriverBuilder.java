package com.atlassian.webdriver;

import com.atlassian.pageobjects.browser.Browser;
import com.browserstack.local.Local;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @see <a href="https://www.browserstack.com/local-testing">Browserstack Local testing</a>
 * @see <a href="https://www.browserstack.com/automate/java">Browserstack Java testing</a>
 */
public class BrowserstackWebDriverBuilder {
    private static final Logger log = LoggerFactory.getLogger(BrowserstackWebDriverBuilder.class);
    private static final String BROWSERSTACK_URL_PATTERN = "https://%s@hub-cloud.browserstack.com/wd/hub";

    private String username;
    private String authkey;
    private String browser;
    private String browserVersion;
    private String os;
    private String osVersion;
    private String projectCategoryName;
    private String testRunName;
    private String buildName;
    private String localIdentifier;
    private Local local;

    public BrowserstackWebDriverBuilder(@Nonnull String username, @Nonnull String authkey) {
        withUser(username);
        withAuthkey(authkey);
    }

    public URL getServerUrl() {
        try {
            final String userInfo = String.format("%s:%s", username, authkey);
            return new URL(String.format(BROWSERSTACK_URL_PATTERN, userInfo));
        } catch (MalformedURLException e) {
            throw new IllegalStateException("Browserstack URL could not be created", e);
        }
    }

    public BrowserstackWebDriverBuilder withBrowser(String browser) {
        this.browser = browser;
        return this;
    }

    public BrowserstackWebDriverBuilder withBrowserVersion(String version) {
        this.browserVersion = version;
        return this;
    }

    public BrowserstackWebDriverBuilder withOS(String os) {
        this.os = os;
        return this;
    }

    public BrowserstackWebDriverBuilder withOSVersion(String osVersion) {
        this.osVersion = osVersion;
        return this;
    }

    public BrowserstackWebDriverBuilder withUser(@Nonnull String user) {
        this.username = user;
        return this;
    }

    public BrowserstackWebDriverBuilder withAuthkey(@Nonnull String authkey) {
        this.authkey = authkey;
        return this;
    }

    public BrowserstackWebDriverBuilder withProjectName(String property) {
        this.projectCategoryName = property;
        return this;
    }

    public BrowserstackWebDriverBuilder withTestName(String property) {
        this.testRunName = property;
        return this;
    }

    public BrowserstackWebDriverBuilder withBuildName(String property) {
        this.buildName = property;
        return this;
    }

    public BrowserstackWebDriverBuilder withLocalConnection(@Nullable Local local) {
        this.local = local;
        return this;
    }

    public BrowserstackWebDriverBuilder withLocalIdentifier(@Nullable String id) {
        this.localIdentifier = id;
        return this;
    }

    public String getLocalIdentifier() {
        return localIdentifier;
    }

    public WebDriverContext build() throws Exception {
        final URL serverUrl = getServerUrl();
        final Browser browserType = Browser.typeOf(browser);

        final DesiredCapabilities capabilities = getBaseCapabilities(browserType);
        final Platform platform = capabilities.getPlatform() != null ? capabilities.getPlatform() : Platform.ANY;
        // set browser metadata
        capabilities.setCapability("browser", capabilities.getBrowserName());
        capabilities.setCapability("browser_version", browserVersion);
        capabilities.setCapability("os", Optional.ofNullable(os).orElse(platform.name()));
        capabilities.setCapability("os_version", Optional.ofNullable(osVersion).orElse(String.valueOf(platform.getMajorVersion())));
        // set metadata for categorising test runs in browserstack's "automate" panel
        capabilities.setCapability("name", testRunName);
        capabilities.setCapability("project", Optional.ofNullable(projectCategoryName).orElse("atlassian-selenium"));
        capabilities.setCapability("build", Optional.ofNullable(buildName).orElseGet(() -> new Date().toString()));
        // enable resolution of URLs via local connection
        if (local != null) {
            final Map<String,String> localOpts = new HashMap<>();
            final String localId = getLocalIdentifier();
            capabilities.setCapability("browserstack.local", "true");
            capabilities.setCapability("browserstack.localIdentifier", localId);
            localOpts.put("key", this.authkey);
            localOpts.put("localIdentifier", localId);
            local.start(localOpts);
        }

        RemoteWebDriver driver = new RemoteWebDriver(serverUrl, capabilities);
        return new DefaultWebDriverContext(driver, browserType);
    }

    static DesiredCapabilities getBaseCapabilities(Browser browserType) {
        final DesiredCapabilities capabilities;
        switch (browserType) {
            default:
            case UNKNOWN:
                log.error("Unknown browser: {}, defaulting to firefox.", browserType);
                // no break here is deliberate.
            case FIREFOX:
                capabilities = DesiredCapabilities.firefox();
                break;
            case CHROME:
                capabilities = DesiredCapabilities.chrome();
                break;
            case EDGE:
                capabilities = DesiredCapabilities.edge();
                break;
            case IE:
                capabilities = DesiredCapabilities.internetExplorer();
                break;
            case IPHONE:
            case IPHONE_SIMULATOR:
                capabilities = DesiredCapabilities.iphone();
                break;
            case IPAD:
                capabilities = DesiredCapabilities.ipad();
                break;
            case ANDROID:
            case ANDROID_EMULATOR:
                capabilities = DesiredCapabilities.android();
                break;
            case SAFARI:
                capabilities = DesiredCapabilities.safari();
                break;
            case OPERA:
                capabilities = DesiredCapabilities.operaBlink();
                break;
        }
        return capabilities;
    }
}
