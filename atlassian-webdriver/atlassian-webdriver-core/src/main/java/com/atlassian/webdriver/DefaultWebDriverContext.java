package com.atlassian.webdriver;

import com.atlassian.pageobjects.browser.Browser;
import com.google.common.base.MoreObjects;
import org.openqa.selenium.WebDriver;

import javax.inject.Inject;

import static java.util.Objects.requireNonNull;

/**
 * Exposes a set of common functions to use.
 */
public class DefaultWebDriverContext implements WebDriverContext {

    private final WebDriver driver;
    private final Browser browser;

    @Inject
    public DefaultWebDriverContext(WebDriver driver, Browser browser)
    {
        this.driver = requireNonNull(driver, "driver");
        this.browser = requireNonNull(browser, "browser");
    }

    @Override
    public Browser getBrowser()
    {
        return browser;
    }

    @Override
    public WebDriver getDriver()
    {
        return driver;
    }

    @Override
    public String toString()
    {
        return MoreObjects.toStringHelper(this)
                .add("driver", driver)
                .add("browser", browser)
                .toString();
    }
}