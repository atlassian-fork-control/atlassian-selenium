package com.atlassian.pageobjects.elements.mock.clock;


import java.time.Clock;
import java.time.Duration;
import java.time.Instant;

import static java.time.ZoneOffset.UTC;

/**
 * {@link Clock} implementations for testing timed queries and conditions.
 *
 */
public final class QueryClocks
{
    private QueryClocks()
    {
        throw new AssertionError("Don't instantiate me");
    }

    public static Clock forInterval(long interval)
    {
        return new CompositeClock(Clock.fixed(Instant.EPOCH, UTC)).addClock(2, new PeriodicClock(Duration.ofMillis(interval), 2));
    }
}
