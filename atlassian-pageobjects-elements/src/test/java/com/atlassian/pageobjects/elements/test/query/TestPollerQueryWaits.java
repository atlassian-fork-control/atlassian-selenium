package com.atlassian.pageobjects.elements.test.query;

import com.atlassian.pageobjects.elements.query.ExpirationHandler;
import com.atlassian.pageobjects.elements.query.Poll;
import com.atlassian.pageobjects.elements.util.IncrementalClock;
import org.junit.Before;
import org.junit.Test;

import java.time.Clock;
import java.time.Instant;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

/**
 * Test case for {@link com.atlassian.pageobjects.elements.query.Poll} with various timed queries.
 * This is the only test that should have a dependancy on the clock. All other would be time dependant tests
 * should mock this class instead.
 */

public class TestPollerQueryWaits
{
    private static final int DEFAULT_INTERVAL = 100;

    private static final Callable<String> query = new Callable<String>()
    {
        @Override
        public String call() throws Exception
        {
            return "Hello world";
        }
    };

    Poll<String> p;
    IncrementalClock c;

    @Before
    public void setup() {
        c = new IncrementalClock(50);
        p = createDefaultPoller(query, c);
    }

    public Poll<String> createDefaultPoller(Callable<String> query, Clock c) {
        return new Poll<>(query, c)
                .every(50, TimeUnit.MILLISECONDS)
                .until(equalTo("Hello world"))
                .withTimeout(10000, TimeUnit.MILLISECONDS)
                .onFailure(ExpirationHandler.RETURN_NULL);
    }

    @Test
    public void pollShouldPassForPassingQuery()
    {
        String result = p.call();
        assertNotNull(result);
        assertThat(c.millis(), lessThan(10000L));
    }

    @Test
    public void pollShouldFailForFailingQuery_afterAFashion()
    {
        String result = p.until(equalTo("wrong answer!")).call();
        assertNull(result);
        assertThat(c.millis(), greaterThan(10000L));
    }

    @Test
    public void pollShouldSucceed_WhenQueryValueBecomesCorrect()
    {
        /**
         * At a random time before the timeout, change the value to the one expected (i.e. the query passes)
         */
        Random rand = new Random();
        final long x = (long)rand.nextInt(9000);
        final AtomicReference<String> str = new AtomicReference<String>("Hello world!");

        Callable<String> query = new Callable<String>()
        {
            @Override
            public String call() throws Exception
            {
                return str.get();
            }
        };

        c = new IncrementalClock(50) {
            public Instant instant() {
                Instant t = super.instant();
                if(t.toEpochMilli() > x) {
                    str.set("right answer!");
                }
                return t;
            }
        };
        p = createDefaultPoller(query, c);

        String result = p.until(equalTo("right answer!")).call();
        assertNotNull(result);
        assertThat(c.millis(), lessThanOrEqualTo(10000L));
    }

    @Test
    public void pollShouldSucceed_WhenQueryValueBecomesCorrect_evenAtTheLastMilliSec()
    {
        /**
         * Right before the timeout, change the value to the one expected (i.e. the query passes)
         */
        final long x = 10000;
        final AtomicReference<String> str = new AtomicReference<String>("Hello world!");

        Callable<String> query = new Callable<String>()
        {
            @Override
            public String call() throws Exception
            {
                return str.get();
            }
        };

        c = new IncrementalClock(50) {
            public Instant instant() {
                Instant t = super.instant();
                if(t.toEpochMilli() > x) {
                    str.set("right answer!");
                }
                return t;
            }
        };
        p = createDefaultPoller(query, c);

        String result = p.until(equalTo("right answer!")).call();
        assertNotNull(result);
        assertThat(c.millis(), greaterThanOrEqualTo(10000L));
    }

}
