package com.atlassian.pageobjects.elements;

import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import org.openqa.selenium.By;

import java.util.Iterator;
import java.util.List;

/**
 * Iterable of PageElements defined by the criteria given to the constructor.
 * 
 * Each call to iterator() returns a fresh list.
 * @since 2.1
 */
public class PageElementIterableImpl<T extends PageElement> implements Iterable<T>
{
    private final PageElementFinder finder;
    private final Class<T> fieldType;
    private final By by;
    private final TimeoutType timeoutType;

    public PageElementIterableImpl(PageElementFinder finder, Class<T> fieldType, By by, TimeoutType timeoutType)
    {
        super();
        this.finder = finder;
        this.fieldType = fieldType;
        this.by = by;
        this.timeoutType = timeoutType;
    }

    public Iterator<T> iterator()
    {
        List<T> pageElements = finder.findAll(by, fieldType, timeoutType);
        return pageElements.iterator();
    }
}
