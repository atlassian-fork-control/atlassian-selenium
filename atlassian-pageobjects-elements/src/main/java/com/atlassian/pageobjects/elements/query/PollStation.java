package com.atlassian.pageobjects.elements.query;

import com.google.common.annotations.VisibleForTesting;

import java.time.Clock;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

// a factory object for Polls
/**
 * @deprecated in 3.0 for removal in 4.0 without replacement
 */
@Deprecated
public class PollStation
{
    long interval;
    long timeout;
    Clock clock;
    ExpirationHandler expirationHandler;

    @VisibleForTesting
    public PollStation(Clock clock)
    {
        this.clock = clock;
    }

    private PollStation()
    {
        this.interval = 50;
        this.timeout = 10000;
        this.expirationHandler = ExpirationHandler.THROW_ASSERTION_ERROR;
        this.clock = Clock.systemUTC();
    }

    public void setInterval(long interval, TimeUnit unit)
    {
        this.interval = TimeUnit.MILLISECONDS.convert(interval, unit);
    }

    public void setTimeout(long time, TimeUnit unit)
    {
        if (time < 0)
        {
            throw new IllegalArgumentException("Timeout must be a positive value.");
        }
        this.timeout = TimeUnit.MILLISECONDS.convert(time, unit);
    }

    public void setExpirationHandler(ExpirationHandler expirationHandler)
    {
        this.expirationHandler = expirationHandler;
    }

    public <T> Poll<T> poll(Callable<T> query)
    {
        return new Poll<T>(query, clock)
                .withTimeout(timeout, TimeUnit.MILLISECONDS)
                .every(interval, TimeUnit.MILLISECONDS)
                .onFailure(expirationHandler);
    }


}
